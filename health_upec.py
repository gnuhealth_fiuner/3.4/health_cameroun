# -*- coding: utf-8 -*-
##############################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#    Copyright (C) 2008-2018 Luis Falcon <lfalcon@gnusolidario.org>
#    Copyright (C) 2011-2018 GNU Solidario <health@gnusolidario.org>
#    Copyright (C) 2015 Cédric Krier
#    Copyright (C) 2014-2015 Chris Zimmerman <siv@riseup.net>
#
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import date

from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool
from trytond.pyson import Eval, Bool, Not, Equal

__all__ = ['PatientUPEC','PatientUpecMolecule']


class PatientUPEC(ModelView, ModelSQL):
    'Patient UPEC Evaluations'
    __name__ = 'gnuhealth.patient.upec'
    
    
    
    patient = fields.Many2One('gnuhealth.patient', 'Patient', required=True)

    ref = fields.Function(
        fields.Char('PUID'),'get_ref')
    
   
    evaluation_date = fields.Date("Evaluation date", required=True)
    
    freq = fields.Selection([
        (None,''),
        ('ordinary','Ordinary'),
        ('systematic','Systematic'),
        ('stable', 'Stable'),            
        ],'Frequency', sort=False,
            states=
                {
                 'required':Equal(Eval('states'),'initiated'),
                 'readonly':Not(Equal(Eval('states'),'initiated')),
                 })

    treatment_line = fields.Selection([
        (None, ''),
        ('one','1'),
        ('two','2'),
        ('three','3'),
        ],'Treatment line', sort = False, 
            states=
                {
                 'required':Equal(Eval('states'),'initiated'),
                 'readonly':Not(Equal(Eval('states'),'initiated')),
                 })
    
    bottles = fields.Integer('Number of bottles')
    
    gender = fields.Function(fields.Char('Gender'),'get_gender')

    phone = fields.Function(fields.Char('Phone'), 'get_phone')
    
    dob = fields.Function(fields.Date('Date of birth'),'get_dob')
    
    age_at_evaluation = fields.Function(
        fields.Integer('Age at Evaluation'),'get_age_at_evaluation')

    hometown = fields.Function(fields.Char('Hometown'),'get_hometown')
    
    dsle = fields.Function(
        fields.Integer('Days since last evaluation'),'get_dsle')
   
    next_evaluation_date = fields.Date('Next Evaluation Date',
                                     states=
                                        {
                                        'required':Equal(Eval('states'),'initiated'),
                                        'readonly':Not(Equal(Eval('states'),'initiated')),
                                        })
    
    dtne = fields.Function(
        fields.Integer('Days to next evaluation'),'get_dtne')

    molecule = fields.Many2One('gnuhealth.patient.upec.molecule','Molecule',
                               states=
                               {
                                'required':Equal(Eval('states'),'initiated'),
                                'readonly':Not(Equal(Eval('states'),'initiated')),
                                })
    observations = fields.Text('Observations')

    states = fields.Selection([        
        (None,''),
        ('draft','Draft'),
        ('initiated','Initiated'),
        ('planned','Planned'),
        ('done','Done'),
        ],'State',sort=False, readonly=True)
    
    def get_ref(self, name=None):
        if self.patient:
            return self.patient.name.ref
    
    @fields.depends('patient')
    def on_change_with_ref(self, name=None):
        if self.patient:
            return self.patient.name.ref
    
    def get_gender(self, name):
        if self.patient and self.patient.gender:
            return self.patient.gender
        return ''

    def get_phone(self,name):
        if self.patient.name.contact_mechanisms:
            return self.patient.name.contact_mechanisms[0].value
        return ''        

    def get_dob(self,name=None):
        if self.patient and self.patient.dob:
            return self.patient.name.dob

    @fields.depends('patient')
    def on_change_with_dob(self, name):
        if self.patient and self.patient.dob:
            return self.patient.name.dob       

    def get_age_at_evaluation(self,name):
        years = 0
        if self.evaluation_date and self.patient and self.patient.dob:
            years = self.evaluation_date.year - self.patient.dob.year
        return years        

    @fields.depends('evaluation_date','patient')
    def on_change_with_age_at_evaluation(self,name=None):
        years = 0
        if self.evaluation_date and self.patient and self.patient.dob:
            years = self.evaluation_date.year - self.patient.dob.year
        return years

    def get_hometown(self,name):
        if self.patient and self.patient.name.du:
            return self.patient.name.du.name
        return ''
    
    @fields.depends('patient')
    def on_change_with_hometown(self,name=None):
        if self.patient.du and self.patient.du.hometown:
            return self.patient.du.hometown

    def get_dsle(self, name=None):
        pool = Pool()
        UpecEval = pool.get('gnuhealth.patient.upec')
        days = 0
        if self.patient and self.evaluation_date:
            upecEvals = UpecEval.search([('patient','=',self.patient.id),
                                         ('evaluation_date','<',self.evaluation_date)])
            if upecEvals:
                last_date_eval = max([x.evaluation_date for x in upecEvals])
                days = (self.evaluation_date-last_date_eval).days
        return days
   
    @fields.depends('patient','evaluation_date')
    def on_change_with_dsle(self, name=None):
        pool = Pool()
        UpecEval = pool.get('gnuhealth.patient.upec')
        days = 0
        if self.patient and self.evaluation_date:
            upecEvals = UpecEval.search([('patient','=',self.patient.id),
                                         ('evaluation_date','<',self.evaluation_date)])
            if upecEvals:
                last_date_eval = max([x.evaluation_date for x in upecEvals])
                days = (self.evaluation_date-last_date_eval).days
        return days
    
    def get_dtne(self, name=None):
        pool = Pool()
        UpecEval = pool.get('gnuhealth.patient.upec')
        days = 0
        if self.patient and self.evaluation_date:
            upecEvals = UpecEval.search([('patient','=',self.patient.id),
                                         ('evaluation_date','>',self.evaluation_date)])
            if upecEvals:
                last_date_eval = min([x.evaluation_date for x in upecEvals])
                days = (last_date_eval-self.evaluation_date).days                
        return days

    @fields.depends('patient','evaluation_date')
    def on_change_with_dtne(self, name=None):
        pool = Pool()
        UpecEval = pool.get('gnuhealth.patient.upec')
        days = 0
        if self.patient and self.evaluation_date:
            upecEvals = UpecEval.search([('patient','=',self.patient.id),
                                         ('evaluation_date','>',self.evaluation_date)])
            if upecEvals:
                last_date_eval = min([x.evaluation_date for x in upecEvals])
                days = (last_date_eval-self.evaluation_date).days                
        return days

    @staticmethod
    def default_evaluation_date():
        return date.today()

class PatientUpecMolecule(ModelView,ModelSQL):
    'Patient Upec Molecule'
    __name__ = 'gnuhealth.patient.upec.molecule'
    
    active = fields.Boolean('Active')
    
    name = fields.Char('Name', required=True)
    
    code = fields.Char('Code')
    
    description = fields.Text('Description')
    
    def get_rec_name(self, name):
        if self.name:
            return self.name

    @staticmethod
    def default_active():
        return True

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('code',) + tuple(clause[1:]),
            (cls._rec_name,) + tuple(clause[1:]),
            ]

    
