# -*- coding: utf-8 -*-
##############################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#    Copyright (C) 2008-2018 Luis Falcon <falcon@gnu.org>
#    Copyright (C) 2011-2018 GNU Solidario <health@gnusolidario.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import pytz
from datetime import datetime
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.report import Report

__all__ = ['UpecReport']


class UpecReport(Report):
    __name__ = 'upec.report'
    
    @classmethod
    def get_context(cls, records, data):
       pool = Pool()
       UpecPatient = pool.get('gnuhealth.patient.upec')
       
       context = super(UpecReport,cls).get_context(records,data)
       
       start_date = data['start_date']
       end_date = data['end_date']
       
       upecPatients = UpecPatient.search([('evaluation_date','>',start_date),
                                          ('evaluation_date','<',end_date)])
       context['upecPatients'] = upecPatients
       
       return context
