# -*- coding: utf-8 -*-
#This file is part health_sisa_census module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
import sys
import unicodedata
from uuid import uuid4
import string
import random

from datetime import datetime

from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateTransition, StateAction, \
    Button
from trytond.pool import Pool
from trytond.pyson import Eval, Bool, Not
from trytond.transaction import Transaction


__all__ = ['PatientUpdateManual']

class PatientUpdateManual(Wizard):
    'Patient Manual'
    __name__ = 'gnuhealth.patient.update.manual'

    start = StateView('gnuhealth.patient.create.manual.start',
        'health_cameroun.view_patient_update_manual_start', [
            Button('Update', 'update_manually', 'tryton-ok'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
        
    def default_start(self, fields):
        pool = Pool()
        Patient = pool.get('gnuhealth.patient')        
        res = {}
        patients = Patient(Transaction().context['active_id'])

        if not patients:
            return res
        res = {
            'name': patients.name.name,
            'lastname': patients.name.lastname,
            'dob': patients.name.dob,
            'puid': patients.name.ref,
            'gender': patients.name.gender,
            'marital_status': patients.name.marital_status,
            'financial_situation': patients.financial_situation,
            'phone': patients.name.contact_mechanisms and patients.name.contact_mechanisms[0].value,
            'phone_contact': patients.name.contact_mechanisms and patients.name.contact_mechanisms[0].remarks,
            'emergency_phone': patients.name.contact_mechanisms and patients.name.contact_mechanisms[0].emergency,
            'phone_aux': patients.name.contact_mechanisms[1].value\
                if len(patients.name.contact_mechanisms) > 1 else None,
            'phone_aux_contact': patients.name.contact_mechanisms[1].remarks\
                if len(patients.name.contact_mechanisms) > 1 else None,
            'emergency_phone_aux': patients.name.contact_mechanisms[1].emergency\
                if len(patients.name.contact_mechanisms) > 1 else None,
            'occupation': patients.name.occupation and patients.name.occupation.id,
            'update_occupation': True,
            'education': patients.name.education,
            'update_education': True,
            'operational_sector': patients.name.du and patients.name.du.operational_sector\
                and patients.name.du.operational_sector.id,
            'update_operational_sector': True,
            'photo': patients.invisible_photo or patients.photo,
            'patient_id': patients.id,
            }    
        return res

    update_manually = StateAction(
        'health.action_gnuhealth_patient_view')

    def do_update_manually(self, action):
        pool = Pool()        
        Patient = pool.get('gnuhealth.patient')
        Contact_mechanisms = pool.get('party.contact_mechanism')
        DU = pool.get('gnuhealth.du')
        SES = pool.get('gnuhealth.ses.assessment')

        patients = Patient.search([('id','=',self.start.patient_id)])       

        for patient in patients:
            patient.name.name = self.start.name
            patient.name.lastname = self.start.lastname
            patient.name.ref = self.start.puid
            patient.name.gender = self.start.gender
            patient.name.marital_status = self.start.marital_status
            patient.name.financial_situation = self.start.financial_situation
            patient.name.dob = self.start.dob
            patient.name.education = self.start.education
            patient.name.occupation = self.start.occupation
            patient.financial_situation: self.start.financial_situation
            patient.name.photo = self.start.photo            
            patient.invisible_photo = self.start.photo
            if patient.name.contact_mechanisms and patient.name.contact_mechanisms[0]:
                patient.name.contact_mechanisms[0].value = self.start.phone
                patient.name.contact_mechanisms[0].remarks = self.start.phone_contact
                patient.name.contact_mechanisms[0].emergency = self.start.emergency_phone
                patient.name.contact_mechanisms[0].save()
            else:
                Contact_mechanisms.create([{
                    'type': 'mobile',
                    'value': self.start.phone,
                    'remarks': self.start.phone_contact,
                    'emergency':self.start.emergency_phone,
                    'party': patient.name.id,            
                    }])
            if patient.name.contact_mechanisms and len(patient.name.contact_mechanisms)>1:
                patient.name.contact_mechanisms[1].value = self.start.phone_aux
                patient.name.contact_mechanisms[1].remarks = self.start.phone_aux_contact
                patient.name.contact_mechanisms[1].emergency = self.start.emergency_phone_aux
                patient.name.contact_mechanisms[1].save()
            elif self.start.phone_aux:
                Contact_mechanisms.create([{
                    'type': 'mobile',
                    'value': self.start.phone_aux,
                    'remarks': self.start.phone_aux_contact,
                    'emergency':self.start.emergency_phone_aux,
                    'party': patient.name.id,
                    }])
            patient.name.save()
            patient.save()

            if not self.start.update_operational_sector:                                
                if patient.name.du:                    
                    du = DU.search([('name','=',patient.name.du.name)])
                    if du:
                        patient.name.du.operational_sector = self.start.operational_sector.id
                        patient.name.du.save()
                else:
                    patient.name.du = DU.create([{
                           'name': str(self.start.operational_sector.name)+' '+self.start.name+' '
                                    + self.start.lastname
                                    +' '+ str(self.start.dob)+' '+self.start.puid,
                            'operational_sector':self.start.operational_sector.id,
                            }])[0]                    

            if not self.start.update_operational_sector or not self.start.update_education\
                or not self.start.update_occupation:
                ses = SES.create([{
                    'education': self.start.education,
                    'occupation': self.start.occupation,
                    'du': patient.name.du and patient.name.du.id,
                    'patient': patient.id,
                    }])
                SES.end_assessment(ses)
            
            patient.name.save()
            patient.save()

        data = {'res_id': [p.id for p in patients]}
        if len(patients) == 1:
            action['views'].reverse()
        return action, data
