# -*- coding: utf-8 -*-
#This file is part health_sisa_census module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
import sys
import unicodedata
from uuid import uuid4
import string
import random

from datetime import datetime
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateTransition, StateAction, \
    Button
from trytond.pool import Pool
from trytond.pyson import Eval, Bool, Not

__all__ = ['PatientCreateManualStart',
            'PatientCreateManual']

class PatientCreateManualStart(ModelView):
    'Patient Create Manual'
    __name__ = 'gnuhealth.patient.create.manual.start'
    
    name = fields.Char("Name", required=True)
    lastname = fields.Char("Lastname", required=True)    
    puid = fields.Char("IDUP", required=True)
    patient_id = fields.Integer('Patient id')
    financial_situation = fields.Selection([
        (None,''),        
        ('social','Social'),
        ('debtor','Debtor'),
        ('pia','Institution Personal')
        ],'Financial Situation ', sort=False)
    gender = fields.Selection([
        ('m', 'Male'),
        ('f', 'Female'),
        ], "Gender", required=True)
    marital_status = fields.Selection([
        (None, ''),
        ('s', 'Single'),
        ('m', 'Married'),
        ('c', 'Concubinage'),
        ('w', 'Widowed'),
        ('d', 'Divorced'),
        ('x', 'Separated'),
        ], 'Marital Status', sort=False,
        help="Marital Status", required=True)
    dob = fields.Date("Birth date", required=True)    
    is_newborn = fields.Boolean("New born")
    mother = fields.Many2One('gnuhealth.patient',"Mother",
                states={
                    'invisible':Not(Bool(Eval('is_newborn'))),
                    'required': Bool(Eval('is_newborn')),          
                    })
    phone = fields.Char("Telephone", required=True)
    phone_contact = fields.Char("Telephone contact")
    emergency_phone = fields.Boolean("Emergency phone")
    phone_aux = fields.Char("Telephone Aux", 
                states={
                    'invisible':Not(Bool(Eval('phone'))),
                    })
    phone_aux_contact = fields.Char("Telephone Aux Contact",
            states={
                    'invisible':Not(Bool(Eval('phone'))),
                    },
            help="Contact name or social reason")
    emergency_phone_aux = fields.Boolean("Emergency phone aux",
            states={
                'invisible':Not(Bool(Eval('phone'))),
                    })
    
    name = fields.Char('Code', required=True)
    desc = fields.Char('Desc')
    address_street = fields.Char('Street')
    address_street_number = fields.Integer('Number')
    address_street_bis = fields.Char('Apartment')
    
    
    operational_sector = fields.Many2One(
            'gnuhealth.operational_sector',
            "Operational Sector", required = True,
            states={
                'readonly': Bool(Eval('update_operational_sector')),
                },
            help="Neighboard")
    operational_area = fields.Function(fields.Char('Operational Area'), 
                                       'get_operational_area')
    
    update_operational_sector = fields.Boolean("Update operational")
    occupation = fields.Many2One('gnuhealth.occupation', 'Occupation',
            states={
                'readonly': Bool(Eval('update_occupation')),
                })
    update_occupation = fields.Boolean("Update occupation")
    education = fields.Selection([
        (None, ''),
        ('0', 'None'),
        ('1', 'Incomplete Primary School'),
        ('2', 'Primary School'),
        ('3', 'Incomplete Secondary School'),
        ('4', 'Secondary School'),
        ('5', 'University'),
        ], 'Education', help="Education Level", sort=False,
            states={
                'readonly': Bool(Eval('update_education')),
                })
    update_education = fields.Boolean("Update occupation")

    photo = fields.Binary('Picture')
                
    @fields.depends('is_newborn','mother')
    def on_change_mother(self):
        STRSIZE = 9
        puid = ''
        if self.is_newborn and self.mother:
            for x in range(STRSIZE):
                if ( x < 3 or x > 5 ):
                    puid = puid + random.choice(string.ascii_uppercase)
                else:
                    puid = puid + random.choice(string.digits)
            self.puid = u"NN fils de "+self.mother.name.ref+ ' ' + '-' + puid
    
    @fields.depends('operational_sector')
    def on_change_with_operational_area(self):
        if self.operational_sector and self.operational_sector.operational_area:
            return self.operational_sector.operational_area.name
        return ''
    
    def get_operational_area(self):
        if self.operational_sector and self.operational_sector.operational_area:
            return self.operational_sector.operational_area.name
        return ''

class PatientCreateManual(Wizard):
    'Patient Create'
    __name__ = 'gnuhealth.patient.create.manual'

    start = StateView('gnuhealth.patient.create.manual.start',
        'health_cameroun.view_patient_create_manual_start', [
            Button('Create', 'create_manually', 'tryton-ok'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])

    create_manually = StateAction(
        'health.action_gnuhealth_patient_view')

    def default_start(self, fields):
        STRSIZE = 9
        puid = ''
        for x in range(STRSIZE):
            if ( x < 3 or x > 5 ):
                puid = puid + random.choice(string.ascii_uppercase)
            else:
                puid = puid + random.choice(string.digits)
        res = {
            'puid': puid
            }
        return res

    def do_create_manually(self, action):
        pool = Pool()
        Party = pool.get('party.party')
        Patient = pool.get('gnuhealth.patient')
        DU = pool.get('gnuhealth.du')
        SES = pool.get('gnuhealth.ses.assessment')
        Date = pool.get('ir.date')

        du = [{
                'name': str(self.start.operational_sector.name)+' '+self.start.name+' '
                        + self.start.lastname
                        +' '+ str(self.start.dob)+' '+self.start.puid,
                'operational_sector':self.start.operational_sector.id,
                }]
        du = DU.create(du)
        
        contact_mechanisms = [{
            'type': 'mobile',
            'value': self.start.phone,
            'remarks': self.start.phone_contact,
            'emergency': self.start.emergency_phone,
            }]
        if self.start.phone_aux:
            contact_mechanisms.append({
                'type': 'mobile',
                'value': self.start.phone_aux,
                'remarks': self.start.phone_aux_contact,
                'emergency': self.start.emergency_phone_aux,
                })
                
        party_data = {
            'name': self.start.name,
            'lastname': self.start.lastname,
            'ref': self.start.puid,            
            'dob': self.start.dob,
            'gender': self.start.gender,
            'marital_status': self.start.marital_status,
            'is_person': True,
            'is_patient': True,
            'photo': self.start.photo,
            'federation_account': self.start.puid,
            'du': du[0].id,
            'contact_mechanisms': [('create',contact_mechanisms)],
            'occupation': self.start.occupation,
            'education': self.start.education,
            'addresses':  [('create',[{'name':'',}])],
            }

        party = Party.create([party_data])
        patient = None
        if party:
            patient = Patient.create([{
                'name': party[0],
                'invisible_photo': self.start.photo,
                'financial_situation': self.start.financial_situation,
                }])

        if self.start.education or self.start.occupation:
            ses = SES.create([{
                'assessment_date': datetime.now(),
                'occupation': self.start.occupation,
                'education': self.start.education,
                'patient': patient[0].id,
                }])
            SES.end_assessment(ses)

        data = {'res_id': [p.id for p in patient]}
        if len(patient) == 1:
            action['views'].reverse()
        return action, data
