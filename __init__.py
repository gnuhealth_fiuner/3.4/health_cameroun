# -*- coding: utf-8 -*-
#This file is part health_sisa_census module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
from trytond.pool import Pool
from .health_cameroun import *
from .health_gyneco import *
from .health_upec import *

from .report import *
from .wizard import *


def register():
    Pool.register(
        Party,
        PatientData,
        PatientCreateManualStart,
        PrenatalEvaluation,
        PatientUPEC,
        PatientUpecMolecule,
        UpecReportCreateStart,
        module='health_cameroun', type_='model')
    Pool.register(
        PatientCreateManual,
        PatientUpdateManual,
        UpecReportCreate,
        module='health_cameroun', type_='wizard')
    Pool.register(
        UpecReport,
        module='health_cameroun', type_='report')
