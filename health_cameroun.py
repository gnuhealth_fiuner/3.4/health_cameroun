# -*- coding: utf-8 -*-
##############################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#    Copyright (C) 2008-2018 Luis Falcon <lfalcon@gnusolidario.org>
#    Copyright (C) 2011-2018 GNU Solidario <health@gnusolidario.org>
#    Copyright (C) 2015 Cédric Krier
#    Copyright (C) 2014-2015 Chris Zimmerman <siv@riseup.net>
#
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Bool, Not


__all__ = ['Party','PatientData']

class Party(metaclass=PoolMeta):
    __name__ = 'party.party'
    
    education = fields.Selection([
        (None, ''),
        ('0', 'None'),
        ('1', 'Incomplete Primary School'),
        ('2', 'Primary School'),
        ('3', 'Incomplete Secondary School'),
        ('4', 'Secondary School'),
        ('5', 'University'),
        ], 'Education Level', help="Education Level", sort=False)
    education_string = education.translated('education')    
    
    marital_status = fields.Selection([
        (None, ''),
        ('s', 'Single'),
        ('m', 'Married'),
        ('c', 'Concubinage'),
        ('w', 'Widowed'),
        ('d', 'Divorced'),
        ('x', 'Separated'),
        ], 'Marital Status', sort=False)
    marital_status_string= marital_status.translated('marital_status')

    delta_dob_years = fields.Integer('Delta years',
                    states={
                        'invisible': Not(Bool(Eval('dob'))),
                        })
    
    def person_age(self, name):
        res = super(Party,self).person_age(name)
        if res:
            years = res[:res.index('y')]
            months = res[res.index('y')+1:res.index('m')]
            days = res[res.index('m')+1:res.index('d')]
            if self.delta_dob_years:            
                return years + ' ans+/- ' + str(delta_dob_years)
            res = years +' ans'+ months + ' mois'+ days + ' jours'
        return res
    
    @staticmethod
    def default_delta_dob_years():
        return 0
        
        
class PatientData(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient'

    ses_resume = fields.Function(fields.Text('SES resume'),
                                 'get_ses_resume')
    
    invisible_photo = fields.Binary('Invisible photo')

    financial_situation = fields.Selection([
        (None,''),
        ('social','Social'),
        ('debtor','Debtor'),
        ('pia','Institution Personal')
        ],'Financial Situation ', sort=False)

    financial_situation_icon = fields.Function(
        fields.Char('Finacial Situation Icon'), 'get_financial_situation')

    def get_ses_resume(self, name):
        pool = Pool()
        marital_status = self.name.marital_status_string and self.name.marital_status_string + "\n" or ''            
        du_os = self.name.du and self.name.du.operational_sector and\
            self.name.du.operational_sector.name + "\n" or ''

        du_oa = self.name.du and self.name.du.operational_sector and\
            self.name.du.operational_sector.operational_area and\
            self.name.du.operational_sector.operational_area.name + "\n" or ''
        
        phone =''
        phone2 =''
        if self.name.contact_mechanisms:
            phone = self.name.contact_mechanisms[0].type
            phone += self.name.contact_mechanisms[0].value and': '+ self.name.contact_mechanisms[0].value\
                or ''
            phone += self.name.contact_mechanisms[0].remarks and ', '+ self.name.contact_mechanisms[0].remarks\
                or ''
            phone += '(<!>)' if self.name.contact_mechanisms[0].emergency else ''
            phone += '\n'
            if len(self.name.contact_mechanisms)>1:
                phone2 = self.name.contact_mechanisms[1].type
                phone2 += self.name.contact_mechanisms[1].value and': '+ self.name.contact_mechanisms[1].value\
                    or ''
                phone2 += self.name.contact_mechanisms[1].remarks and ', '+ self.name.contact_mechanisms[1].remarks\
                    or ''
                phone2 += '(<!>)' if self.name.contact_mechanisms[1].emergency else ''
                phone2 += '\n'
                
        receivable = str(self.name.receivable) +'\n' if self.name.receivable else ''
        education = self.name.education_string and self.name.education_string + "\n" or ''
        occupation = self.name.occupation and self.name.occupation.name + "\n" or ''
        ses_resume = du_os + du_oa + education + occupation + marital_status + phone + phone2\
            + receivable
        
        return ses_resume

    def get_financial_situation(self, name):
        if self.financial_situation == 'social':
            return 'financial_situation_blue'
        elif self.financial_situation == 'debtor':
            return 'financial_situation_red'
        elif self.financial_situation == 'pia':
            return 'financial_situation_pia'
        return ''

class Appointment(metaclass=PoolMeta):
    __name__ = 'gnuhealth.appointment'
    
    
    @classmethod
    def __setup__(cls):
        super(Appointment,cls).__setup__()
        cls.states.selection.append(('enregistrement','Enregistrement'))
        

class DomiciliaryVisit(ModelSQL, ModelView):
    "Domiciliary Visits Records"
    __name__ = "gnuhealth.dv"
    
    name = fields.Char('Visit description',help='Description of the domiciliary visit')
